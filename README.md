# ATC-156 controller library

Ametek's JOFRA ATC-156 controller library.

Supported functions:

* Logging on
* Logging off
* Changing to remote mode.
* Getting the maximum temperature.
* Getting the minimum temperature.
* Getting the serial number.
* Getting the current (real) temperature and checking if it is stabilized.
* Setting the precision.
* Setting the unit (°C, °F, °K).
* Setting the temperature.

## Communication

The library only uses the stream it receives as a parameter.
It doesn't open or close it.

### Initialize

Call the logon `'Logon()'` function to start the communication.

Optionally change to change to remote `'Remote()'` mode for write/set support.

 ### Get/set values

After the initialization we can get or set values.
The set functions work only after switching to remote mode.

### Finishing

When we done, we should log off `'Logoff()'`.

## Serial port

However the application only uses a stream, the device has a serial port.
The default parameters to set up the communication are the following:

```
Parity = None 
DataBits = 8
StopBits = 1
BaudRate = 9600
Handshake = None
```
