﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using imperfect.devices.atc.command;
using imperfect.devices.atc.model;

namespace imperfect.devices.atc
{
	internal delegate void Setter();
	
	public class Atc : IDisposable
	{
		private const int MaxRetry = 3;
		private const int ReadTimeout = 1000;
		
		private readonly Stream stream;

		public string ManufacturerName => "Ametek";
		
		public Atc(Stream stream)
		{
			this.stream = stream;
		}
		
		public float GetMaximumTemperature() =>
			GetterWithRetry(() => new GetMaximumTemperature(stream).Execute());
		
		public float GetMinimumTemperature() =>
			GetterWithRetry(() => new GetMinimumTemperature(stream).Execute());

		public string GetSerialNumber() =>
			GetterWithRetry(() => new GetSerialNumber(stream).Execute());

		public Temperature GetTemperature() =>
			GetterWithRetry(() => new GetTemperature(stream).Execute());

		public void Logoff()
		{
			SetterWithRetry(() => new Logoff(stream).Execute());
			Trace.TraceInformation($"[{typeof(Atc)}]: Logged off.");
		}

		public Instrument Logon()
		{
			var versionData = GetterWithRetry(() => new Logon(stream).Execute());
			Trace.TraceInformation($"[{typeof(Atc)}]: Logged on.");
			Trace.TraceInformation($"[{typeof(Atc)}]: Instrument type: {versionData.Instrument}.");
			Trace.TraceInformation($"[{typeof(Atc)}]: Protocol version: {versionData.ProtocolVersion}.");
			Trace.TraceInformation($"[{typeof(Atc)}]: Software version: {versionData.SoftwareVersion}.");
			return versionData.Instrument;
		}

		public void Remote()
		{
			SetterWithRetry(() => new Remote(stream).Execute());
			Trace.TraceInformation($"[{typeof(Atc)}]: Remote mode.");
		}

		public void SetPrecision(Precision precision)
		{
			SetterWithRetry(() => new SetPrecision(stream).Execute(precision));
			Trace.TraceInformation($"[{typeof(Atc)}]: Precision set to 1/10^{precision}.");
		}
		
		public void SetTemperature(float temperature)
		{
			SetterWithRetry(() => new SetTemperature(stream).Execute(temperature));
			Trace.TraceInformation($"[{typeof(Atc)}]: Temperature set to {temperature}.");
		}

		public void SetUnit(Unit unit)
		{
			SetterWithRetry(() => new SetUnit(stream).Execute(unit));
			Trace.TraceInformation($"[{typeof(Atc)}]: Unit set to {unit}.");
		}

		private TR GetterWithRetry<TR>(Func<TR> function)
		{
			int iteration = 0;
			Exception exception = new TimeoutException("Implementation error");
			while (iteration < MaxRetry)
			{
				try
				{
					return function.Invoke();
				}
				catch (Exception ex)
				{
					exception = ex;
					Thread.Sleep(ReadTimeout);
				}
				iteration++;
			}
			throw exception;
		}
		
		private void SetterWithRetry(Setter function)
		{
			int iteration = 0;
			Exception exception = null;
			bool succeeded = false;
			while (!succeeded && iteration < MaxRetry)
			{
				try
				{
					function.Invoke();
					exception = null;
					succeeded = true;
				}
				catch (Exception ex)
				{
					exception = ex;
					succeeded = false;
					Thread.Sleep(ReadTimeout);
				}
				iteration++;
			}

			if (!succeeded && exception != null)
			{
				throw exception;
			}
		}

		public void Dispose() => Logoff();
	}
}
