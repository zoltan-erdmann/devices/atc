﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.atc
{
	internal static class Constants
	{
		public const int ReadBufferSize = 128;
		public const int TelegramMinLength = 5;
		
		public static readonly byte[] TelegramEnd = { 0x04 };
	}
}
