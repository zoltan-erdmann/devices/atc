﻿// SPDX-License-Identifier: LGPL-3.0-only

using System.IO;
using System.Linq;
using imperfect.devices.atc.model;
using imperfect.devices.atc.util;

using static imperfect.devices.atc.Constants;
using static imperfect.devices.atc.util.Assertions;
using static imperfect.devices.atc.util.Packer;

namespace imperfect.devices.atc.command
{
	internal class GetTemperature
	{
		private const ushort Id = 0x03;
		
		private readonly Stream stream;
		
		public GetTemperature(Stream stream)
		{
			this.stream = stream;
		}

		public Temperature Execute()
		{
			byte[] bytes = CreateTelegramId(Id).ConcatCrc().Escape().Concat(TelegramEnd).ToArray();
			stream.Write(bytes, 0, bytes.Length);
			
			byte[] buffer = new byte[ReadBufferSize];
			int readBytes = stream.Read(buffer, 0, buffer.Length);
			var escapedTelegram = buffer.Take(readBytes).ToArray();
			byte[] unescapedTelegram = escapedTelegram.SkipLast(1)
				.Unescape().Concat(escapedTelegram.TakeLast(1)).ToArray();
			
			AssertLenght(TelegramMinLength
			             + 6 * sizeof(float)
			             + 3 * sizeof(byte)
			             + 2 * sizeof(short)
			             + 2 * sizeof(byte),
				unescapedTelegram);
			AssertTelegramId(Id, unescapedTelegram);
			AssertCrc(unescapedTelegram);
			AssertTelegramEnd(unescapedTelegram);
			
			var dataBytes = unescapedTelegram.Skip(2).SkipLast(3).ToArray();

			return new Temperature
			{
				Reference = dataBytes.Skip(4).AsSingle(),
				IsStabilized = dataBytes.Skip(25).AsBoolean()
			};
		}
	}
}
