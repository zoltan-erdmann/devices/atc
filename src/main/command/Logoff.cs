﻿// SPDX-License-Identifier: LGPL-3.0-only

using System.IO;
using System.Linq;
using imperfect.devices.atc.util;

using static imperfect.devices.atc.Constants;
using static imperfect.devices.atc.util.Assertions;
using static imperfect.devices.atc.util.Packer;

namespace imperfect.devices.atc.command
{
	internal class Logoff
	{
		private const ushort Id = 2;
		
		private readonly Stream stream;
		
		public Logoff(Stream stream)
		{
			this.stream = stream;
		}
		
		public void Execute()
		{
			byte[] bytes = CreateTelegramId(Id).ConcatCrc().Escape().Concat(TelegramEnd).ToArray();
			stream.Write(bytes, 0, bytes.Length);
			
			byte[] buffer = new byte[ReadBufferSize];
			int readBytes = stream.Read(buffer, 0, buffer.Length);
			var escapedTelegram = buffer.Take(readBytes).ToArray();
			byte[] unescapedTelegram = escapedTelegram.SkipLast(1)
				.Unescape().Concat(escapedTelegram.TakeLast(1)).ToArray();
			
			AssertLenght(TelegramMinLength, unescapedTelegram);
			AssertTelegramId(Id, unescapedTelegram);
			AssertCrc(unescapedTelegram);
			AssertTelegramEnd(unescapedTelegram);
		}
	}
}
