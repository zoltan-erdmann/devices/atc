﻿// SPDX-License-Identifier: LGPL-3.0-only

using System.IO;
using System.Linq;
using imperfect.devices.atc.model;
using imperfect.devices.atc.util;

using static imperfect.devices.atc.Constants;
using static imperfect.devices.atc.util.Assertions;
using static imperfect.devices.atc.util.Packer;

namespace imperfect.devices.atc.command
{
	internal class Logon
	{
		private const ushort Id = 1;
		
		private readonly Stream stream;
		
		public Logon(Stream stream)
		{
			this.stream = stream;
		}

		public VersionData Execute()
		{
			byte[] bytes = CreateTelegramId(Id).ConcatCrc().Escape().Concat(TelegramEnd).ToArray();
			stream.Write(bytes, 0, bytes.Length);
			
			byte[] buffer = new byte[ReadBufferSize];
			int readBytes = stream.Read(buffer, 0, buffer.Length);
			var escapedTelegram = buffer.Take(readBytes).ToArray();
			byte[] unescapedTelegram = escapedTelegram.SkipLast(1)
				.Unescape().Concat(escapedTelegram.TakeLast(1)).ToArray();
			
			AssertLenght(TelegramMinLength + 3 * sizeof(ushort), unescapedTelegram);
			AssertTelegramId(Id, unescapedTelegram);
			AssertCrc(unescapedTelegram);
			AssertTelegramEnd(unescapedTelegram);
			
			var dataBytes = unescapedTelegram.Skip(2).SkipLast(3).ToArray();
			
			return new VersionData
			{
				Instrument = (Instrument) dataBytes.AsUInt16(),
				ProtocolVersion = dataBytes.Skip(2).AsVersionString(),
				SoftwareVersion = dataBytes.Skip(4).AsVersionString()
			};
		}
	}
}
