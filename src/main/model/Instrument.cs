﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.atc.model
{
	public enum Instrument : ushort
	{
		Atc155A = 0x0bcd, // 3021
		Atc320A = 0x0bce, // 3022
		Atc650A = 0x0bcf, // 3023
		Atc156A = 0x0bd0, // 3024
		Atc157A = 0x0bd1, // 3025
		Atc125A = 0x0bd2, // 3026
		Atc140A = 0x0bd3, // 3027
		Atc250A = 0x0bd4, // 3028
		Atc155B = 0x0c31, // 3121
		Atc320B = 0x0c32, // 3122
		Atc650B = 0x0c33, // 3123
		Atc156B = 0x0c34, // 3124
		Atc157B = 0x0c35, // 3125
		Atc125B = 0x0c36, // 3126
		Atc140B = 0x0c37, // 3127
		Atc250B = 0x0c38, // 3128
	}
}
