﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.atc.model
{
	public enum Precision : byte
	{
		One = 0x00,
		Tenths = 0x01,
		Hundredths = 0x02
	}
}
