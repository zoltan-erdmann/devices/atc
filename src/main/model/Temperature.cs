﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.atc.model
{
	public record Temperature
	{
		public float Reference { get; init; }
		public bool IsStabilized { get; init; }
	}
}
