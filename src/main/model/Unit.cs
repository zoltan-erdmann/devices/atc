﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.atc.model
{
	public enum Unit : byte
	{
		Celsius = 0x00,
		Fahrenheit = 0x01,
		Kelvin = 0x02
	}
}
