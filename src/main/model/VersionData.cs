﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace imperfect.devices.atc.model
{
	internal record VersionData
	{
		public Instrument Instrument { get; init; }
		public string ProtocolVersion { get; init; }
		public string SoftwareVersion { get; init; }
	}
}
