﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using static imperfect.devices.atc.Constants;

namespace imperfect.devices.atc.util
{
	internal static class Assertions
	{
		public static void AssertLenght(int expectedLength, byte[] bytes)
		{
			if (expectedLength != bytes.Length)
			{
				throw new InvalidConstraintException("Wrong data length");
			}
		}
		
		public static void AssertTelegramEnd(byte[] bytes)
		{
			if (TelegramEnd.Last() != bytes.Last())
			{
				throw new InvalidConstraintException("Wrong data length");
			}
		}
		
		public static void AssertTelegramId(ushort expectedTelegramId, IEnumerable<byte> unescapedTelegram)
		{
			if (expectedTelegramId != unescapedTelegram.Take(2).AsUInt16())
			{
				throw new InvalidConstraintException("Telegram Id mismatch");
			}
		}
		
		public static void AssertCrc(byte[] bytes)
		{
			var crcBytes = bytes[^3..^1].Reverse().ToArray();
			var receivedCrc = BitConverter.ToUInt16(crcBytes, 0);
			var calculatedCrc = Checksum.CalculateCrc(bytes[..^3].ToArray());
			if(calculatedCrc != receivedCrc)
			{
				throw new InvalidConstraintException("CRC values don't match");
			}
		}
	}
}
