﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.Collections.Generic;
using System.Linq;

namespace imperfect.devices.atc.util
{
	internal static class Checksum
	{

		public static IEnumerable<byte> ConcatCrc(this IEnumerable<byte> source)
		{
			if (source is null || !source.Any())
			{
				throw new InvalidOperationException("Cannot calculate checksum for a null or empty set.");
			}

			var crcBytes = BitConverter.GetBytes(source.Aggregate<byte, ushort>(0, CalculateCrc));
			return source.Concat(crcBytes.Reverse());
		}
		
		public static ushort CalculateCrc(IEnumerable<byte> bytes) =>
			bytes.Aggregate<byte, ushort>(0, CalculateCrc);
		
		private static ushort CalculateCrc(ushort crcSum, byte dataChar) {
			crcSum = (ushort)(crcSum ^ (ushort)(dataChar * 256));
			for(byte count = 0; count <= 7; count++) {
				if(crcSum > 0x7FFF) {
					crcSum = (ushort)((crcSum * 2) ^ 0x8005);
				} else {
					crcSum = (ushort)(crcSum * 2);
				}
			}
			return crcSum;
		}
	}
}
