﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace imperfect.devices.atc.util
{
	internal static class Converter
	{
		public static float AsSingle(this IEnumerable<byte> source)
		{
			if (source is null || !source.Any())
			{
				throw new InvalidOperationException("Cannot convert null or empty set to float");
			}
			return BitConverter.ToSingle(source.Take(4).Reverse().ToArray());
		}
		
		public static ushort AsUInt16(this IEnumerable<byte> source)
		{
			if (source is null || !source.Any())
			{
				throw new InvalidOperationException("Cannot convert null or empty set to short");
			}
			return BitConverter.ToUInt16(source.Take(2).Reverse().ToArray());
		}
		
		public static bool AsBoolean(this IEnumerable<byte> source)
		{
			if (source is null || !source.Any())
			{
				throw new InvalidOperationException("Cannot convert null or empty set to bool");
			}
			return BitConverter.ToBoolean(source.Take(1).ToArray());
		}

		public static string AsVersionString(this IEnumerable<byte> source)
		{
			if (source is null || !source.Any())
			{
				throw new InvalidOperationException("Cannot convert null or empty set to version string");
			}
			return (source.Take(2).Reverse().AsUInt16() / 100f).ToString("0.00", CultureInfo.InvariantCulture);
		}

		public static string AsAsciiString(this IEnumerable<byte> source)
		{
			if (source is null || !source.Any())
			{
				throw new InvalidOperationException("Cannot convert null or empty set to string");
			}

			return Encoding.ASCII.GetString(source.TakeWhile(x => x != 0x00).ToArray());
		}

		public static IEnumerable<byte> ConcatSingle(this IEnumerable<byte> source, float value)
		{
			if (source is null)
			{
				throw new InvalidOperationException("Cannot concatenate to null");
			}

			return source.Concat(BitConverter.GetBytes(value).Reverse());
		}
	}
}
