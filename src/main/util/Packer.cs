﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.Collections.Generic;
using System.Linq;

namespace imperfect.devices.atc.util
{
	internal static class Packer
	{
		public static IEnumerable<byte> CreateTelegramId(ushort telegramId) =>
			BitConverter.GetBytes(telegramId).Reverse();

		public static IEnumerable<byte> Escape(this IEnumerable<byte> source)
		{
			if (source is null || !source.Any())
			{
				throw new InvalidOperationException("Cannot escape a null or empty set.");
			}
			List<byte> bytes = new List<byte>(source);
			int pos = 0;
			while(pos < bytes.Count)
			{
				if(bytes[pos] == 0x1B)
				{
					bytes[pos] = 0xE5;
					bytes.Insert(pos, 0x1B);
					pos++;
				}
				else if(bytes[pos] == 0x04)
				{
					bytes[pos] = 0xFC;
					bytes.Insert(pos, 0x1B);
					pos++;
				}
				pos++;
			}

			return bytes;
		}
		
		public static IEnumerable<byte> Unescape(this IEnumerable<byte> source)
		{
			if (source is null || !source.Any())
			{
				throw new InvalidOperationException("Cannot escape a null or empty set.");
			}
			List<byte> bytes = new List<byte>(source);
			int pos = 0;
			int stat = 0;
			while(pos < bytes.Count)
			{
				if(bytes[pos] == 0x1B)
				{
					stat = 1;
					pos++;
				}
				else if(stat == 1 && bytes[pos] == 0xFC)
				{
					bytes[pos - 1] = 0x04;
					bytes.RemoveAt(pos);
					stat = 0;
				}
				else if(stat == 1 && bytes[pos] == 0xE5)
				{
					bytes[pos - 1] = 0x1B;
					bytes.RemoveAt(pos);
					stat = 0;
				}
				else
				{
					stat = 0;
					pos++;
				}
			}

			return bytes;
		}
	}
}
